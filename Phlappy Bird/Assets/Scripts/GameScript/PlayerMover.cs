﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using UnityEngine.UI;

public class PlayerMover : MonoBehaviour {

    [SerializeField] GameObject theUI;
    //Player Mover by Eli Manikan
    //Wind Effect by Rom Arena

    void Start ()
    {
        StartCoroutine(WindEffect());
    }
	
	// Adjusts player's acceleration values when buttons are pressed.
	// Don't crash into stuff. Good luck.
	void Update () {
		DifficultyPlus ();

		if (gameObject.GetComponent<johnRigidBody> ().acceleration.y > 2.0f) {
			gameObject.GetComponent<johnRigidBody>().acceleration.y = 2.0f;
		}

		if (gameObject.GetComponent<johnRigidBody> ().acceleration.y < -2.0f) {
			gameObject.GetComponent<johnRigidBody>().acceleration.y = -2.0f;
		}

		if (gameObject.GetComponent<johnRigidBody> ().acceleration.x > 2.0f) {
			gameObject.GetComponent<johnRigidBody>().acceleration.x = 2.0f;
		}

		if (gameObject.GetComponent<johnRigidBody> ().acceleration.x < -2.0f) {
			gameObject.GetComponent<johnRigidBody>().acceleration.x = -2.0f;
		}
		
		if (Input.GetKey(KeyCode.UpArrow))
		{	
			gameObject.GetComponent<johnRigidBody>().acceleration.y += 0.1f * Time.deltaTime;
		}

		if (Input.GetKey(KeyCode.DownArrow))
		{
			gameObject.GetComponent<johnRigidBody>().acceleration.y -= 0.1f * Time.deltaTime;
		}

		if (Input.GetKey(KeyCode.LeftArrow))
		{
			if (gameObject.GetComponent<johnRigidBody>().acceleration.x > -0.1f)
			{
				gameObject.GetComponent<johnRigidBody>().acceleration.x -= 0.1f * Time.deltaTime;
			}
		}

		if (Input.GetKey(KeyCode.RightArrow))
		{
			if (gameObject.GetComponent<johnRigidBody>().acceleration.x < 0.1f)
			{
				gameObject.GetComponent<johnRigidBody>().acceleration.x += 0.1f * Time.deltaTime;
			}
		}

		//Cheat mode - turns off the player's collider component. 2spooky4me
		if (Input.GetKey(KeyCode.Q))
		{
			Debug.Log ("thank mr skeltal DOOT DOOT");
			gameObject.GetComponent<CollisionTest>().enabled = false;
		}

		FlushMovement ();
	}

	void FlushMovement()
	{
		if (gameObject.GetComponent<johnRigidBody> ().acceleration.x < 0) {
			gameObject.GetComponent<johnRigidBody> ().acceleration.x += 0.01f * Time.deltaTime;
		} else if (gameObject.GetComponent<johnRigidBody> ().acceleration.x > 0) {
			gameObject.GetComponent<johnRigidBody> ().acceleration.x -= 0.01f * Time.deltaTime;
		}

		if (gameObject.GetComponent<johnRigidBody> ().acceleration.y < 0) {
			gameObject.GetComponent<johnRigidBody> ().acceleration.y += 0.01f * Time.deltaTime;
		} else if (gameObject.GetComponent<johnRigidBody> ().acceleration.y > 0) {
			gameObject.GetComponent<johnRigidBody> ().acceleration.y -= 0.01f * Time.deltaTime;
		}
	}

	void DifficultyPlus()
	{
		if (gameObject.GetComponent<johnRigidBody> ().Gravity.x < 0.115) {
			gameObject.GetComponent<johnRigidBody> ().Gravity.x += 0.002f * Time.deltaTime;
		}
		if (gameObject.GetComponent<johnRigidBody> ().Gravity.x > -0.15) {
			gameObject.GetComponent<johnRigidBody> ().Gravity.y -= 0.002f * Time.deltaTime;
		}
	}


//Wind Effect by Rom Arena
    IEnumerator WindEffect()
    {
        float elapsedtime = 0.0f;
        float randomWind = Random.Range(-0.1f, 0.1f);
        while (elapsedtime < 2.0f)
        {
            theUI.GetComponent<Text>().text = randomWind.ToString();
            gameObject.GetComponent<johnRigidBody>().acceleration.x += randomWind * Time.deltaTime;
            elapsedtime += Time.deltaTime;
            //elapsedtime = 0.0f;
            yield return null;

        }
    }
}
