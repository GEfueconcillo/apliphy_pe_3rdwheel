﻿using UnityEngine;
using System.Collections;

[System.Serializable]
public struct johnVector2 
{
    public float x, y;

    public johnVector2(float x, float y)
    {
        this.x = x;
        this.y = y;
    }

    public static johnVector2 operator +(johnVector2 v1,johnVector2 v2)
    {
        return new johnVector2(v1.x + v2.x, v1.y + v2.y);
    }

    public static johnVector2 operator -(johnVector2 v1, johnVector2 v2)
    {
        return new johnVector2(v1.x - v2.x, v1.y - v2.y);
    }

    public static johnVector2 operator *(johnVector2 v1, float v2)
    {
        return new johnVector2(v1.x * v2, v1.y * v2);
    }
    public static johnVector2 operator *(float v1, johnVector2 v2)
    {
        return new johnVector2(v2.x * v1, v2.y * v1);
    }

    public static johnVector2 operator /(johnVector2 v1, float v2)
    {
        return new johnVector2(v1.x / v2, v1.y / v2);
    }

    public static johnVector2 operator /(float v1 , johnVector2 v2)
    {
        return new johnVector2(v2.x / v1, v2.y / v1);
    }




}
