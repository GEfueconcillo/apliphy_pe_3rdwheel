using UnityEngine;
using System.Collections;

public class johnRigidBody : MonoBehaviour 
{
	public johnVector2 Position;
	public johnVector2 Gravity = new johnVector2(0, 0);
	public float Mass;
	
	public johnVector2 velocity;
	public johnVector2 acceleration;
	public johnVector2 storedForce;

	//All RigidBodies are assumed to be affected by gravity.
	public bool IsKinematic;

	void Awake()
	{
		Position = new johnVector2(transform.position.x, transform.position.y);
	}
	
	void FixedUpdate()
	{
		if (!IsKinematic) {
			velocity += Gravity;
		}
		velocity += (acceleration / Mass);
		
		ConvertForce();
		MovebyVelocity();
		ChangeTransform();
	}
	
	public void ChangeTransform()
	{
		gameObject.transform.position = new Vector3(Position.x, Position.y, transform.position.z);
	}
	
	public void ConvertForce()
	{
		velocity += (Mass * storedForce);
		storedForce = new johnVector2(0, 0);
	}
	
	public void AddForce(johnVector2 ForceToAdd)
	{
		storedForce += ForceToAdd;
	}
	
	public void MovebyVelocity()
	{
		Position += velocity;
		velocity = new johnVector2 (0.0f, 0.0f);
	}

	public void EditPosition(float x, float y)
	{
		Position.x += x;
		Position.y += y;
	}
}


