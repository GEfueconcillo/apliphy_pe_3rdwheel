﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class CollisionManager : MonoBehaviour
{
	//CollisionManager by Eli Manikan.

	public GameObject Player;
	public List<GameObject> colliders = new List<GameObject> ();
		
	void Start ()
	{
		//Populate the list with objects the player can collide with.
		//Anything that has the "Enemy" or "Platform" tag fits.
		colliders.AddRange (GameObject.FindGameObjectsWithTag ("Collider"));
	}

	void Update ()
	{
		//Fixed update to ensure it's the last thing that happens.
		//Kind of like YGO?
		//Also regular update taxes the system and we'll be running through a
		//potentially gigantic list so eh.
		CheckCollision ();
	}

	void CheckCollision ()
	{
		//Run through the list of all possible colliders, checking if the player is colliding with anything.
		foreach (GameObject bump in colliders) {
			//If something does collide, evaluate that collision.
			if (Player.GetComponent<CollisionTest> ().AABB (bump, Player)) {
				EvaluateColliderCollision (bump);
			}
		}
	}

	void EvaluateColliderCollision (GameObject platform)
	{	
		//Where A checks for collision with something below,
		//B checks for collision with something above.
		//C checks for collision from the left
		//and D checks for collision on the right.
		float a = Player.GetComponent<CollisionTest> ().LowerLeft.y - 
			platform.GetComponent<CollisionTest> ().UpperRight.y;

		float b = Player.GetComponent<CollisionTest> ().UpperRight.y - 
			platform.GetComponent<CollisionTest> ().LowerLeft.y;

		float c = Player.GetComponent<CollisionTest> ().UpperRight.x - 
			platform.GetComponent<CollisionTest> ().LowerLeft.x;
			
		float d = Player.GetComponent<CollisionTest> ().LowerLeft.x - 
			platform.GetComponent<CollisionTest> ().UpperRight.x;

		//Is it more likely that the objects will collide vertically? Then check for vertical collision.
		//If the objects are more likely to collide horizontally, ignore the potential vertical collision.
		//Normally, the static collider would repel the player a certain distance.
		//But in this case, the player dies if they crash, aka hit a static collider.
		if (Mathf.Abs (a + b) > Mathf.Abs (c + d)) {
			if (Mathf.Abs (a) < Mathf.Abs (b)) {
				Application.LoadLevel(Application.loadedLevel);
				Player.GetComponent<johnRigidBody> ().EditPosition (0, 0.2f);
				return;
			} else if (Mathf.Abs (b) < Mathf.Abs (a)) {
				Application.LoadLevel(Application.loadedLevel);
				Player.GetComponent<johnRigidBody> ().EditPosition (0, -0.2f);
				return;
			}
		} else if (Mathf.Abs (a + b) < Mathf.Abs (c + d)) {
			if (Mathf.Abs (c) < Mathf.Abs (d)) {
				Application.LoadLevel(Application.loadedLevel);
				Player.GetComponent<johnRigidBody> ().EditPosition (-0.2f, 0);
				return;
			} else if (Mathf.Abs (d) < Mathf.Abs (c)) {
				Application.LoadLevel(Application.loadedLevel);
				Player.GetComponent<johnRigidBody> ().EditPosition (0.2f, 0);
				return;
			} else {
				return;
			}
		}
	}
}