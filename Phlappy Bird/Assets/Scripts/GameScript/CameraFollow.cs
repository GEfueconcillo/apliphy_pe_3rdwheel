﻿using UnityEngine;
using System.Collections;

public class CameraFollow : MonoBehaviour {

	private float xmagnet;

	// Use this for initialization
	void Start () {

	}
	
	// Update is called once per frame
	void Update () {
		xmagnet = GameObject.FindGameObjectWithTag ("Player").GetComponent<johnRigidBody> ().Gravity.x;
		transform.position += new Vector3 (0.75f + xmagnet, 0, 0) * Time.deltaTime;
	}
}
