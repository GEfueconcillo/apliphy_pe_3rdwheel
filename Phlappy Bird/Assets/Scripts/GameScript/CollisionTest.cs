﻿ using UnityEngine;
using System.Collections;

public class CollisionTest : MonoBehaviour {
	//CollisionTest by Eli Manikan.

	public johnVector2 LowerLeft;
	public johnVector2 UpperRight;
	public Renderer Rend;
	public GameObject DebugMarker;
	private GameObject currentmarker;

	void Start () {
		//Use Renderer component to draw BBox.
		Rend = GetComponent<Renderer> ();
		currentmarker = Instantiate (DebugMarker, new Vector3 (Rend.bounds.min.x, Rend.bounds.min.y, 0), Quaternion.identity) as GameObject;
		Adopt (currentmarker);
		currentmarker = Instantiate (DebugMarker, new Vector3 (Rend.bounds.max.x, Rend.bounds.max.y, 0), Quaternion.identity) as GameObject;
		Adopt (currentmarker);
		currentmarker = Instantiate (DebugMarker, new Vector3 (Rend.bounds.min.x, Rend.bounds.max.y, 0), Quaternion.identity) as GameObject;
		Adopt (currentmarker);
		currentmarker = Instantiate (DebugMarker, new Vector3 (Rend.bounds.max.x, Rend.bounds.min.y, 0), Quaternion.identity) as GameObject;
		Adopt (currentmarker);

		//Peg BBox coordinates.
		LowerLeft.x = Rend.bounds.min.x;
		LowerLeft.y = Rend.bounds.min.y;
		UpperRight.x = Rend.bounds.max.x;
		UpperRight.y = Rend.bounds.max.y;
	}

	void Update () {
		//And again. This doesn't work as a function.
		LowerLeft.x = Rend.bounds.min.x;
		LowerLeft.y = Rend.bounds.min.y;
		UpperRight.x = Rend.bounds.max.x;
		UpperRight.y = Rend.bounds.max.y;
	}

	//Purely a debug function, so don't put in API?
	//Attaches object 
	void Adopt(GameObject baby) {
		baby.transform.parent = gameObject.transform;
	}

	public bool AABB(GameObject A, GameObject B)
	{
		//AABB collision detection
		float d1x = B.GetComponent<CollisionTest> ().LowerLeft.x - 
			A.GetComponent<CollisionTest> ().UpperRight.x;

		float d1y = B.GetComponent<CollisionTest> ().LowerLeft.y - 
			A.GetComponent<CollisionTest> ().UpperRight.y;

		float d2x = A.GetComponent<CollisionTest> ().LowerLeft.x - 
			B.GetComponent<CollisionTest> ().UpperRight.x;

		float d2y = A.GetComponent<CollisionTest> ().LowerLeft.y - 
			B.GetComponent<CollisionTest> ().UpperRight.y;

		if (d1x > 0 || d2x > 0 || d1y > 0 || d2y > 0) {
			return false;
		}
		return true;
	}
}