﻿using UnityEngine;
using System.Collections;

public class Rotation : MonoBehaviour {
	//by Gerard Fueconcillo

	public johnVector2 testAngle = new johnVector2 (0, 0);


	public int Degrees;



	void Update() 
	{	
		Vector3 rotating = new Vector3 (testAngle.x, testAngle.y, 10);
		Quaternion rotation = Quaternion.LookRotation (rotating);
		Quaternion current = transform.localRotation;

		if(Input.GetKey(KeyCode.UpArrow) && Degrees <= 75)  
		{
			transform.rotation = Quaternion.Slerp (current, rotation, 0.01f * Time.deltaTime);
			Degrees++;
		}

		if(Input.GetKey(KeyCode.DownArrow) && Degrees >= -75)
		{
			transform.rotation = Quaternion.Slerp (current, rotation, 0.01f * Time.deltaTime);
			Degrees--;
		}
			
	}
}
