﻿using UnityEngine;
using System.Collections;

public struct johnCollider
{

    public johnVector2
        PointA,
        PointB;

    public johnCollider (johnVector2 PointA, johnVector2 PointB)
    {
        this.PointA = PointA;
        this.PointB = PointB;
    }

    bool IsColliding(johnCollider other)
    {
        if (PointA.x > other.PointA.x && PointA.x < other.PointB.x && PointA.y > other.PointB.y && PointA.y < other.PointA.y)
            return true;
        if (PointB.x > other.PointA.x && PointB.x < other.PointB.x && PointB.y > other.PointB.y && PointB.y < other.PointA.y)
            return true;
        if (PointA.x > other.PointA.x && PointA.x < other.PointB.x && PointB.y > other.PointB.y && PointB.y < other.PointA.y)
            return true;
        if (PointB.x > other.PointA.x && PointB.x < other.PointB.x && PointA.y > other.PointB.y && PointA.y < other.PointA.y)
            return true;

        return false;
    }

    


	
}
